import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OrderMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton ramenButton;
    private JButton gyudonButton;
    private JButton udonButton;
    private JButton hamburgerButton;
    private JButton sobaButton;
    private JButton chineseDumplingButton;
    private JTextPane orderedBox;
    private JButton checkoutButton;
    private JLabel orderedLabel;
    private JLabel totalLabel;
    int total=0;

    public static void main(String[] args) {
        JFrame frame = new JFrame("OrderMachine");
        frame.setContentPane(new OrderMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order(String foodName,int price){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order "+ foodName +"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if(confirmation == 0){
            total += price;
            String currentText = orderedBox.getText();
            orderedBox.setText(currentText + foodName + " ("+ price + "yen)\n");
            JOptionPane.showMessageDialog(null,"Thank you for ordering " + foodName + ".");
            totalLabel.setText("Total         "+total+"yen");
        }
    }

    void orderAndSize(String foodName,int price){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order "+ foodName +"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if(confirmation == 0){
            String currentText = orderedBox.getText();
            orderedBox.setText(currentText + foodName + " ("+ price + "yen)\n");
            JOptionPane.showMessageDialog(null,"Thank you for ordering " + foodName + ".");
            Object[] orderSizeOptions = {"large(+70yen)","middle","half(-70yen)"};
            int orderSize = JOptionPane.showOptionDialog(
                    null,
                    "which size would you like to choose?",
                    "Choose Size",
                    JOptionPane.DEFAULT_OPTION,
                    JOptionPane.INFORMATION_MESSAGE,
                    null,
                    orderSizeOptions,
                    orderSizeOptions[0]
            );
            if(orderSize == 0){
                int sizeConfirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to order large size?",
                        "size Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(sizeConfirmation == 0){
                    String currentText1 = orderedBox.getText();
                    orderedBox.setText(currentText1 + "large(+70yen)\n");
                    JOptionPane.showMessageDialog(null,"You ordered large size.");
                    total += price+70;
                }else{
                    String currentText1 = orderedBox.getText();
                    orderedBox.setText(currentText1 + "↑ Canceled\n");
                    price=0;
                    total+=price;
                }
            }else if(orderSize == 1){
                int sizeConfirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to order middle size?",
                        "size Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(sizeConfirmation == 0){
                    String currentText1 = orderedBox.getText();
                    orderedBox.setText(currentText1 + "middle\n");
                    JOptionPane.showMessageDialog(null,"You ordered middle size.");
                    total = total;
                }else{
                    String currentText1 = orderedBox.getText();
                    orderedBox.setText(currentText1 + "↑ Canceled\n");
                    price=0;
                    total+=price;
                }
            }else if(orderSize == 2){
                int sizeConfirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to order half size?",
                        "size Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(sizeConfirmation == 0){
                    String currentText1 = orderedBox.getText();
                    orderedBox.setText(currentText1 + "half(-70yen)\n");
                    JOptionPane.showMessageDialog(null,"You ordered half size.");
                    total = price-70;
                }else{
                    String currentText1 = orderedBox.getText();
                    orderedBox.setText(currentText1 + "↑ Canceled\n");
                    price=0;
                    total+=price;
                }
            }
            totalLabel.setText("Total         "+total+"yen");
        }
    }

    void orderAndSideMenu(String foodName,int price){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order "+ foodName +"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if(confirmation == 0){
            total += price;
            String currentText = orderedBox.getText();
            orderedBox.setText(currentText + foodName + " ("+ price + "yen)\n");
            JOptionPane.showMessageDialog(null,"Thank you for ordering " + foodName + ".");
            Object[] sideMenuOptions = {"french fries","chicken nuggets","salad","nothing"};
            int sideMenu = JOptionPane.showOptionDialog(
                    null,
                    "which side menu would you like to choose?(Each costs an additional 50yen.)",
                    "Choose Side Menu",
                    JOptionPane.DEFAULT_OPTION,
                    JOptionPane.INFORMATION_MESSAGE,
                    null,
                    sideMenuOptions,
                    sideMenuOptions[0]
            );
            if(sideMenu == 0){
                int sideMenuConfirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to order french fries?",
                        "side menu Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(sideMenuConfirmation == 0){
                    String currentText1 = orderedBox.getText();
                    orderedBox.setText(currentText1 + "french fries(+50yen)\n");
                    JOptionPane.showMessageDialog(null,"You ordered french fries.");
                    total += 50;
                }else{
                    String currentText1 = orderedBox.getText();
                    orderedBox.setText(currentText1 + "↑ Canceled\n");
                    price=0;
                    total+=price;
                }
            }else if(sideMenu == 1){
                int sideMenuConfirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to order chicken nuggets?",
                        "side menu Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(sideMenuConfirmation == 0){
                    String currentText1 = orderedBox.getText();
                    orderedBox.setText(currentText1 + "chicken nuggets(+50yen)\n");
                    JOptionPane.showMessageDialog(null,"You ordered chicken nuggets.");
                    total += 50;
                }else{
                    String currentText1 = orderedBox.getText();
                    orderedBox.setText(currentText1 + "↑ Canceled\n");
                    price=0;
                    total+=price;
                }
            }else if(sideMenu == 2){
                int sideMenuConfirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to order salad?",
                        "side menu Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(sideMenuConfirmation == 0){
                    String currentText1 = orderedBox.getText();
                    orderedBox.setText(currentText1 + "salad(+50yen)\n");
                    JOptionPane.showMessageDialog(null,"You ordered salad.");
                    total += 50;
                }else{
                    String currentText1 = orderedBox.getText();
                    orderedBox.setText(currentText1 + "↑ Canceled\n");
                    price=0;
                    total+=price;
                }
            }else if(sideMenu == 3){
                int sideMenuConfirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like any side menu?",
                        "side menu Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(sideMenuConfirmation == 0){
                    String currentText1 = orderedBox.getText();
                    total = total;
                }
            }
            totalLabel.setText("Total         "+total+"yen");
        }
    }

    public OrderMachine() {
        this.ramenButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                orderAndSize("Ramen",700);
            }
        });
        this.ramenButton.setIcon(new ImageIcon(this.getClass().getResource("ramen.jpg")));

        this.udonButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                orderAndSize("Udon",500);
            }
        });
        this.udonButton.setIcon(new ImageIcon(this.getClass().getResource("udon.jpg")));

        this.sobaButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                orderAndSize("Soba",480);
            }
        });
        this.sobaButton.setIcon(new ImageIcon(this.getClass().getResource("soba.jpg")));

        this.gyudonButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                orderAndSize("Gyudon",450);
            }
        });
        this.gyudonButton.setIcon(new ImageIcon(this.getClass().getResource("gyudon.jpg")));

        this.hamburgerButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                orderAndSideMenu("Hamburger",650);
            }
        });
        this.hamburgerButton.setIcon(new ImageIcon(this.getClass().getResource("hamburger.jpg")));

        this.chineseDumplingButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                order("Chinese Dumpling",350);
            }
        });
        this.chineseDumplingButton.setIcon(new ImageIcon(this.getClass().getResource("chinesedumpling.jpg")));

        checkoutButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int checkout_confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(checkout_confirmation == 0){
                    total = 0;
                    orderedBox.setText("");
                    totalLabel.setText("Total      "+total+"yen");
                    JOptionPane.showMessageDialog(null,"Thank you for ordering.We will cook them as soon as possible.");
                }
            }
        });
    }

}
